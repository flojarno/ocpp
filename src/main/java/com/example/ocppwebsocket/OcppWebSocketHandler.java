package com.example.ocppwebsocket;

import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;

public class OcppWebSocketHandler implements WebSocketHandler {

    @Override
    public Mono<Void> handle(WebSocketSession session) {
        return session.send(
                session.receive()
                        .map(msg -> {
                            String request = msg.getPayloadAsText();
                            System.out.println("Received message: " + request);

                            // Simulate an OCPP response
                            String response = "[3,\"" + request + "\",{\"status\":\"Accepted\"}]";
                            return session.textMessage(response);
                        })
        );
    }
}


