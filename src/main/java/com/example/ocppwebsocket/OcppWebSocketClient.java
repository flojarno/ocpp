package com.example.ocppwebsocket;

import com.example.ocppwebsocket.ocpp16.BootNotificationRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.time.Duration;
import java.util.List;

public class OcppWebSocketClient {

    public static void main(String[] args) throws Exception {
        ReactorNettyWebSocketClient client = new ReactorNettyWebSocketClient();

        // Create a BootNotificationRequest object and convert it to JSON string
        BootNotificationRequest bootNotificationRequest = new BootNotificationRequest("VendorX", "ModelY");
        ObjectMapper objectMapper = new ObjectMapper();
        String bootNotificationJson = objectMapper.writeValueAsString(bootNotificationRequest);

        List<String> ocppMessages = List.of(bootNotificationJson
        );


        client.execute(
                URI.create("ws://localhost:9091/ocpp"),
                session -> session.send(
                                Flux.fromIterable(ocppMessages)
                                        .map(session::textMessage)
                                        .delayElements(Duration.ofSeconds(1))
                        ).thenMany(session.receive().take(ocppMessages.size()).map(WebSocketMessage::getPayloadAsText))
                        .doOnNext(response -> System.out.println("Received: " + response))
                        .then()
        ).block(Duration.ofMinutes(1));
    }
}
