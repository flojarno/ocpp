package com.example.ocppwebsocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OcppwebsocketApplication {

	public static void main(String[] args) {
		SpringApplication.run(OcppwebsocketApplication.class, args);
	}

}
