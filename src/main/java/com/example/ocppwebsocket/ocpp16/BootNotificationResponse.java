package com.example.ocppwebsocket.ocpp16;

public class BootNotificationResponse {
    private String status;
    private String currentTime;

    public BootNotificationResponse() {
    }

    public BootNotificationResponse(String status, String currentTime) {
        this.status = status;
        this.currentTime = currentTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }
}
