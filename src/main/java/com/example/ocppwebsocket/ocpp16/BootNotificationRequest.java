package com.example.ocppwebsocket.ocpp16;

public class BootNotificationRequest {
    private String chargePointVendor;
    private String chargePointModel;

    public BootNotificationRequest() {
    }

    public BootNotificationRequest(String chargePointVendor, String chargePointModel) {
        this.chargePointVendor = chargePointVendor;
        this.chargePointModel = chargePointModel;
    }

    public String getChargePointVendor() {
        return chargePointVendor;
    }

    public void setChargePointVendor(String chargePointVendor) {
        this.chargePointVendor = chargePointVendor;
    }

    public String getChargePointModel() {
        return chargePointModel;
    }

    public void setChargePointModel(String chargePointModel) {
        this.chargePointModel = chargePointModel;
    }
}
